# Locality Aware Scheduling

Locality-Aware Scheduling of Independant Tasks for Runtime Systems - Maxime Gonthier, Loris Marchal and Samuel Thibault

All the informations are in this depot:
```bash
~$ git clone --single-branch --branch europar2021 https://gitlab.inria.fr/starpu/locality-aware-scheduling.git
```

You can find in this depot the .pdf of our research report, the code used for our simulations and the code used in Starpu.
You can re-create the curves used in the paper by doing the following steps.

## Install

### To install automatically all the packages needed you can do:

```bash
~/locality-aware-scheduling$ bash Script/Install_packages.sh
```

### In order to genereate the curves you need these packages in R:

```bash
~/locality-aware-scheduling$ R
> install.packages("ggplot2")
> install.packages("tidyverse")
> q()
> n
```

### To install Simgrid for the simulations:

```bash
~/locality-aware-scheduling$ wget https://framagit.org/simgrid/simgrid/uploads/98ec9471211bba09aa87d7866c9acead/simgrid-3.26.tar.gz
~/locality-aware-scheduling$ tar xf simgrid-3.26.tar.gz
~/locality-aware-scheduling$ bash Script/Install_simgrid.sh
```

### To install Starpu

```bash
~/locality-aware-scheduling$ git clone https://gitlab.inria.fr/starpu/starpu.git 
~/locality-aware-scheduling$ cd starpu/
~/locality-aware-scheduling/starpu$ git checkout commit_a_preciser
~/locality-aware-scheduling/starpu$ ./autogen.sh
~/locality-aware-scheduling/starpu$ ./configure --enable-simgrid --disable-mpi
~/locality-aware-scheduling/starpu$ make
~/locality-aware-scheduling/starpu$ sudo make install
~/locality-aware-scheduling/starpu$ sudo ldconfig
~/locality-aware-scheduling/starpu$ export STARPU_PERF_MODEL_DIR=/usr/local/share/starpu/perfmodels/sampling
```

## Running the experiments

Your user's password may be asked to compile with sudo.

There are two type of scripts you can run:
* Experiments_europar.sh restart the tests in starpu of all the curves present in the report. It last approximately 15h.
* Quick_experiments_europar.sh will draw the same curves but without the last 2 or 3 points for some curves. It last approximately 1h and 23minutes.

You might see small differences for the the heuristic random order because we did not used a fixed seed for it.

You have to enter the path in which you have simgrid, starpu and the path of the R folder.
For example if you followed the step above you have: 
```bash
~/locality-aware-scheduling$ cd starpu/
~/locality-aware-scheduling/starpu$ bash Scripts_maxime/Quick_experiments_europar.sh $HOME/locality-aware-scheduling/ $HOME/locality-aware-scheduling/ $HOME/locality-aware-scheduling/
```
Else it is:
```bash
~/locality-aware-scheduling$ cd starpu/
~/locality-aware-scheduling/starpu$ bash Scripts_maxime/Quick_experiments_europar.sh path_simgrid path_starpu path_folder_R
```
or
```bash
~/locality-aware-scheduling$ cd starpu/
~/locality-aware-scheduling$ bash Scripts_maxime/Experiments_europar.sh path_simgrid path_starpu path_folder_R
```

All the graph created will be in the folder: /locality-aware-scheduling/R/Courbes/
